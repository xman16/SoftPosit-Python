# <img src="https://posithub.org/images/NGA_logo.png"  width="90" height="50"/> SoftPosit-Python

This is the python wrapper (using SWIG) of SoftPosit

## Supported on

* 64 bit machines
* Linux and Windows OS

## Installation

Support only on Linux, OS-X and Windows systems with gcc. Please note that you will need to have gcc installed on your system.

Currently we support only 64-bit systems:

Wheels support:

* Windows (Python 3.5-3.7)
* OS-X (Python 2.7 and 3.7)
* Linux (Python 2.7, 3.4-3.7)

For all other versions, you will have to compile the source as a part of pip installation. Consequently for windows and OS-X users, please install Visual Studio Build Tools and Xcode respectively.

Tested on Windows (cl.exe and Mingw-w64) Mac-OSX (CLANG) and Linux SuSE (GNU).

First install the prerequisite:

    pip install requests

### Pip Install

To install using pip (might require root priviledges):

    pip install softposit

To install using pip in user space:

    pip install softposit --user
    
To force it to install the latest version in user space:

    pip install --no-cache-dir softposit --user

### Source Install 

To install via source (might require root priviledges)::

    python setup.py install
    
To install via source in user space:

    python setup.py install --user

To install from source in a specific directory:

    python setup.py install --prefix=YOUR_PREFERRED_DIRECTORY
    
## To use

https://posithub.org/docs/PositTutorial_Part1.html

The easiest way is to use ipython

```
import softposit as sp

#Initialise a 16-bit posit
a = sp.posit16(2.6)
b = a + 2.1
a *= 2
c = b/a

# fused-multiply-add => c + (0.2* 2.3)
c.fma(0.2, 2.3)

result = c.sqrt()

# convert a 16-bit posit to 8-bit posit
p8 = c.toPosit8() 


#quire - initialise to zero when declared
q = sp.quire16()

#fused multiply subtract -> q + (6.2*1.2)
q.qms(6.2, 1.2)

#convert quire to posit
c = q.toPosit()

#to clear quire to zero
q.clr()

#print
print a

```

## Acknowledgement

Special thanks to Shin Yee for his support. If not for his help, this port might take forever!
